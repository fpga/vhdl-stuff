library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library lpm;
use lpm.lpm_components.all;

entity scomp is
    port (
        clock, reset : in std_logic;
        program_counter_out : out std_logic_vector(7 downto 0);
        register_AC_out : out std_logic_vector(15 downto 0);
        memory_data_register_out : out std_logic_vector(15 downto 0)
       );
end scomp;

architecture a of scomp is
	type state_type is (reset_pc, fetch, decode, execute_add, execute_load, execute_store, execute_store3, execute_store2, execute_jump);
	signal state: state_type;
	signal instruction_register, memory_data_register : std_logic_vector(15 downto 0);
	signal register_ac : std_logic_vector(15 downto 0);
	signal program_counter : std_logic_vector(7 downto 0);
	signal memory_address_register : std_logic_vector(7 downto 0);
	signal memory_write, nclock : std_logic;
begin
	--utilisation de la fonction LPM pour la m�moire du compteur (256 16-bits words)
	memory: lpm_ram_dq
    generic map (
		lpm_widthad => 8,
		lpm_outdata => "UNREGISTERED",
		lpm_indata => "REGISTERED",
		lpm_address_control => "REGISTERED",
		--lire dans le fichier mif le programme initial et les valeurs de fichier
		lpm_file =>"program.mif",
		lpm_width => 16
	)      
    port map (
		data => register_ac,
		address => memory_address_register,
		we => memory_write, 
		inclock => nclock,
		q => memory_data_register
	);
    program_counter_out <= program_counter;
    register_ac_out <= register_ac;
    memory_data_register_out <= memory_data_register;
    nclock <= not(clock);
   
	process (clock, reset)
	begin
		if reset='1' then state <= reset_pc;
		elsif clock'event and clock = '1' then
			case state is
				--reset the computer, need to clear some registers
				when reset_pc =>
					program_counter <= "00000000";
					memory_address_register  <= "00000000";
					register_AC <= "0000000000000000";
					memory_write <= '0';
					state	<=fetch;
				--fetch instruction from memory and add 1 to PC
				when fetch => 
					instruction_register <= memory_data_register;
					program_counter <= program_counter +1;
					memory_write <= '0';
					state <= decode;
				--decode instruction and send out adress of any date operands
				when decode => memory_address_register <=instruction_register(7 downto 0);
				case instruction_register(15 downto 8) is
					when "00000000" => state <=execute_add;
					when "00000001" => state <=execute_store;
					when "00000010" => state <=execute_load;
					when "00000011" => state <= execute_jump;
					when others => state <= fetch;
				end case ;
				--Exectute the add instruction 
				when execute_add =>
					register_ac <= register_ac + memory_data_register;
					memory_address_register	<= program_counter;
					state <=fetch;
				--execute the store instruction
				when execute_store =>
					--write register_ac to memory
					memory_write <= '1';
					state	<= execute_store2; --this state ensures that the memory valid until after memory_write goes inactive
				when execute_store2 =>
					memory_write <= '0';
					state	<= execute_store3;
				when execute_store3 => 
					memory_address_register 	<=program_counter;
					state	<=fetch;
				--execute the load instruction
				when execute_load =>
					register_ac <= memory_data_register;
					memory_address_register	<= program_counter;
					state <=fetch;
				--execute the jump instruction
				when execute_jump =>
					memory_address_register <=instruction_register(7 downto 0);
					program_counter <= instruction_register(7 downto 0);
					state	<= fetch;
				when others =>
					memory_address_register	<= program_counter;
					state <= fetch;
			end case;
		end if;
	end process;
end a;