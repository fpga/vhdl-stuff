library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity test is
	port(
		trig : out std_logic;
		echo, clk : in std_logic;
		--echo_time : out std_logic_vector (15 downto 0);
		motor : out std_logic_vector (1 downto 0)
	);
end test;

architecture behave of test is
	signal echo_time : integer;
begin
  process(clk)
    variable c1,c2: integer:=0;
    variable y :std_logic:='1';
  begin
    if rising_edge(clk) then

        if(c1=0) then
            trig<='1';
        elsif(c1=500) then--100us
            trig<='0';
            y:='1';
        elsif(c1=5000000) then-- 100 ms
            c1:=0;
            trig<='1';
        end if;
        c1:=c1+1;

        if(echo = '1') then
            c2:=c2+1;
        elsif(echo = '0' and y='1' ) then-- I change the y to not get echo_time =0;
			echo_time <= c2;
            --echo_time <= std_logic_vector(to_unsigned(c2, echo_time'length));
            c2:=0;
            y:='0';
        end if;

        if(echo_time < 100000) then--20 cm
            motor<="10";
        elsif(echo_time > 150000)then--30 cm
            motor<="01";
        else-- between  
            motor<="11";
        end if;
    end if; 
end process ;
end behave;