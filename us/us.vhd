library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity us is
generic
(
	freq	: integer  :=	50e6
);
port
(
	triger : out std_logic;
	echo, clk : in std_logic;
	d : out std_logic_vector (15 downto 0)
);
end us;

architecture rtl of us is
	signal tick, tickecho, dist : integer;
	signal flag : std_logic;
begin
	process (clk, echo)
	begin
		if (clk'event and clk = '1') then
			tick <= tick + 1;
		end if;
		if (echo = '1') then 
			tickecho <= tickecho + 1;
			flag <= '1';
		elsif (flag = '1' and echo = '0') then
			dist <= tickecho * 100 / 17 / freq; --cm
			flag <= '0';
		end if;
		case tick is
			when 550 => triger <= '0'; -- 11us
			when 3000000 =>  -- 60ms
				tick := 0;
				triger <= '1';
				tickecho := 0;
			when others => null;
		end case;
		d <= std_logic_vector(to_unsigned(dist, 16));
end process;
end rtl;
